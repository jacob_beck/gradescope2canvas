#! /usr/bin/python

"""
gradescope2canvas.py - Script to append gradescope grades to canvas
Jacob Beck - beckjac@oregonstate.edu
"""

import csv
import sys

def main():
    # First arg is original Canvas CSV
    canvas_path = sys.argv[1]

    # Second arg is assignment for which grades are being replaced (must be exact)
    assignment_name = sys.argv[2]

    # Third arg is Gradescope CSV
    gradescope_path = sys.argv[3]
    
    # Open CSV readers
    canvas_reader = csv.reader(open(canvas_path))
    gradescope_reader = csv.reader(open(gradescope_path))

    # Load all data
    canvas_data = [r for r in canvas_reader]
    gradescope_data = [r for r in gradescope_reader]
    
    # Find idx of assigment
    header = canvas_data[0]
    assignment_idx = None
    for idx in xrange(len(header)):
        # Look for name in the beginning of the key
        if header[idx][0:len(assignment_name)] == assignment_name:
            assignment_idx = idx
            break
    
    if assignment_idx == None:
        print "ERROR: Assignment " + assignment_name + " does not appear in Canvas data"
        exit()
    
    # Move grades
    for new_grade in gradescope_data[1:]:
        # Using email as index
        email = new_grade[2]
        
        try:
            old_grade = next(elm for elm in canvas_data if elm[3] == email)
            old_grade[assignment_idx] = new_grade[3]
        except StopIteration:
            print "WARN: Email " + email + " does not appear in Canvas data"
    
    # Write out new grade
    output_path = assignment_name + '(updated).csv'
    canvas_writer = csv.writer(open(output_path, 'w'))
    for row in canvas_data:
        canvas_writer.writerow(row)

if __name__ == '__main__':
    main()
